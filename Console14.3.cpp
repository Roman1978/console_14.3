﻿#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>

int main()
{
    std::cout << "Enter any word: ";
    std::string s;
    char c;
    getline(std::cin, s);
    c = s[0];

    std::cout << c << "\n";
    std::cout << s[s.size() - 1] << "\n";
    std::cout << s.length() << "\n";
   
    std::cin;
    return 0;
}
